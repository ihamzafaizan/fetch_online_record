# -*- coding: utf-8 -*-
import requests
import lxml.html
import xlsxwriter
import xlrd
import threading
import time
from odoo import models, fields, api


class ScrapeData(models.Model):
    _name = 'scrape.data'
    _description = 'Scrape Data'
    _rec_name = 'customer_cnic'

    customer_mobile = fields.Char(string="Customer Mobile")
    customer_name = fields.Char(string="Customer Name")
    customer_cnic = fields.Char(string="Customer CNIC")
    customer_address = fields.Char(string="Customer Address")
    customer_operator = fields.Char(string="Customer Operator")

    def scrape_data(self):
        data = {}  # Phone Number Will be updated.
        # list_of_data = []  # Scraped Data will be appended.

        encoding = 'utf-8'

        headers = {
            'authority': 'pagead2.googlesyndication.com',
            'accept': 'image/avif,image/webp,image/apng,image/svg+xml,image/,/*;q=0.8',
            'accept-language': 'en-US,en;q=0.9',
            'cache-control': 'max-age=0',
            'content-type': 'application/x-www-form-urlencoded',
            'cookie': 'IDE=AHWqTUkFya76HLwvnkrLkVN3PToMYyU3u8s3uRUr13jpFRTDOYufihz3X72MAKHgpPE; DSID=NO_DATA',
            'origin': 'https://simdatabaseonline.com',
            'referer': 'https://simdatabaseonline.com/',
            'sec-ch-ua': '"Chromium";v="106", "Google Chrome";v="106", "Not;A=Brand";v="99"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
            'sec-fetch-dest': 'image',
            'sec-fetch-mode': 'no-cors',
            'sec-fetch-site': 'cross-site',
            'sec-fetch-user': '?1',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
            'Referer': 'https://fonts.googleapis.com/',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
            'Origin': 'https://googleads.g.doubleclick.net',
            'x-client-data': 'CIW2yQEIpbbJAQipncoBCJWhywEIqrzMAQiVvcwBCMXhzAEI/erMAQjw7cwB',
            'If-None-Match': '"cI9qrbJoS0UmqHsLpMVQfnjXBRc="',
            'If-Modified-Since': 'Sat, 05 Nov 2022 08:27:53 GMT',
            'Accept': 'application/json, text/plain, /',
            '$referer': 'https://googleads.g.doubleclick.net/pagead/ads?client=ca-pub-5066680227575398&output=html&h=280&slotname=1175957513&adk=2662987973&adf=2826711444&pi=t.ma~as.1175957513&w=783&fwrn=4&fwrnh=100&lmt=1667655023&rafmt=1&format=783x280&url=https%3A%2F%2Fsimdatabaseonline.com%2Ftele%2Fsearch-result.php&fwr=0&fwrattr=true&rpe=1&resp_fmts=3&wgl=1&uach=WyJMaW51eCIsIjUuMTUuMCIsIng4NiIsIiIsIjEwNi4wLjUyNDkuMTE5IixbXSxmYWxzZSxudWxsLCI2NCIsW1siQ2hyb21pdW0iLCIxMDYuMC41MjQ5LjExOSJdLFsiR29vZ2xlIENocm9tZSIsIjEwNi4wLjUyNDkuMTE5Il0sWyJOb3Q7QT1CcmFuZCIsIjk5LjAuMC4wIl1dLGZhbHNlXQ..&dt=1667655023671&bpp=8&bdt=189&idt=68&shv=r20221101&mjsv=m202211010101&ptt=9&saldr=aa&abxe=1&cookie=ID%3D9276f74c59577397-22800ce067ce0010%3AT%3D1667634654%3ART%3D1667634654%3AS%3DALNI_MZgQq-4yt47FppSGJJuw9AWpjJlBA&gpic=UID%3D00000b7cb82f9619%3AT%3D1667634654%3ART%3D1667634654%3AS%3DALNI_MYLHDcXxUa3exs8F6_DJCKeRxCdAA&correlator=6085378611830&frm=20&pv=2&ga_vid=1717899663.1667634639&ga_sid=1667655024&ga_hid=1382268715&ga_fc=1&u_tz=300&u_his=13&u_h=1080&u_w=1920&u_ah=983&u_aw=1920&u_cd=24&u_sd=1.25&dmc=8&adx=228&ady=193&biw=1524&bih=393&scr_x=0&scr_y=0&eid=44759875%2C44759926%2C44759842%2C42531706%2C31070616%2C31070664%2C44775017%2C44777830&oid=2&pvsid=976828468861327&tmod=1959629943&uas=0&nvt=1&ref=https%3A%2F%2Fsimdatabaseonline.com%2Ftele%2Fsearch.php&eae=0&fc=896&brdim=0%2C27%2C0%2C27%2C1920%2C27%2C1920%2C983%2C1536%2C393&vis=1&rsz=%7C%7CpeE%7C&abl=CS&pfx=0&fu=128&bc=31&ifi=1&uci=a\\u00211&fsb=1&xpc=xDzUZ4Alef&p=https%3A//simdatabaseonline.com&dtd=128',
        }

        vals = {}

        with open('/home/odoo/Programming/python/Nadra Record/sim_data.txt') as perm:
            no_of_rows = perm.readlines()[1:50000]  # Read Range of Rows.

            for phone_number in no_of_rows:
                # if self.env['scrape.data'].search([('customer_mobile', '=', phone_number)]):
                #     pass
                # else:
                phone_number = phone_number.replace('\n', "")
                update_data = {'num': phone_number}
                data.update(update_data)

                # Post Phone Number and get data in 'repsonse'.
                response = requests.post('https://simdatabaseonline.com/tele/search-result.php', headers=headers,
                                         data=data)

                if response.status_code == 200:
                    html_content = (response.content).decode(encoding)  # html page converted into string.
                    lxml_content = lxml.html.fromstring(html_content)  # string converted to lxml.

                    record_check = lxml_content.xpath('//div[@class="card-body"]//span')[2].text

                    if record_check != ' ':
                        print('No Record Found against - ', phone_number)

                    else:
                        # record_check = int(lxml_content.xpath('//h1//span')[5].text)
                        # if record_check >= 1:
                        # Record 1
                        customer_mobile = str(
                            lxml_content.xpath('//td[@class="tg-iu1i"]//span')[0].text)
                        customer_name = str(
                            lxml_content.xpath('//td[@class="tg-de5r"]//span')[0].text)
                        customer_cnic = str(
                            lxml_content.xpath('//td[@class="tg-xfje"]//span')[0].text)
                        customer_address = str(
                            lxml_content.xpath('//td[@class="tg-wgsn"]//span')[0].text)
                        customer_operator = str(
                            lxml_content.xpath('//td[@class="tg-0pky"]//span')[4].text)

                        vals = {
                            'customer_mobile': customer_mobile,
                            'customer_name': customer_name,
                            'customer_cnic': customer_cnic,
                            'customer_address': customer_address,
                            'customer_operator': customer_operator
                        }
                        if vals:
                            # if self.env['scrape.data'].search([('customer_mobile', '=', customer_mobile)]):
                            #     print('Record in Odoo Db Exists!')
                            #     pass
                            # else:
                            new_record = self.env['scrape.data'].create(vals)
                            # print(vals)
                        # Record 2
                        record_check = int(lxml_content.xpath('//h1//span')[5].text)
                        if record_check >= 2:
                            customer_mobile = str(
                                lxml_content.xpath('//td[@class="tg-iu1i"]//span')[1].text)
                            customer_name = str(
                                lxml_content.xpath('//td[@class="tg-de5r"]//span')[1].text)
                            customer_cnic = str(
                                lxml_content.xpath('//td[@class="tg-xfje"]//span')[1].text)
                            customer_address = str(
                                lxml_content.xpath('//td[@class="tg-wgsn"]//span')[1].text)
                            customer_operator = str(
                                lxml_content.xpath('//td[@class="tg-0pky"]//span')[9].text)

                            vals_2 = {
                                'customer_mobile': customer_mobile,
                                'customer_name': customer_name,
                                'customer_cnic': customer_cnic,
                                'customer_address': customer_address,
                                'customer_operator': customer_operator
                            }

                            if vals_2:
                                new_record = self.env['scrape.data'].create(vals_2)
                                # print(vals_2)
                        # Record 3
                        record_check = int(lxml_content.xpath('//h1//span')[5].text)
                        if record_check >= 3:
                            customer_mobile = str(
                                lxml_content.xpath('//td[@class="tg-iu1i"]//span')[2].text)
                            customer_name = str(
                                lxml_content.xpath('//td[@class="tg-de5r"]//span')[2].text)
                            customer_cnic = str(
                                lxml_content.xpath('//td[@class="tg-xfje"]//span')[2].text)
                            customer_address = str(
                                lxml_content.xpath('//td[@class="tg-wgsn"]//span')[2].text)
                            customer_operator = str(
                                lxml_content.xpath('//td[@class="tg-0pky"]//span')[14].text)

                            vals_3 = {
                                'customer_mobile': customer_mobile,
                                'customer_name': customer_name,
                                'customer_cnic': customer_cnic,
                                'customer_address': customer_address,
                                'customer_operator': customer_operator
                            }

                            if vals_3:
                                new_record = self.env['scrape.data'].create(vals_3)
                                # print(vals_3)
                        # Record 4
                        record_check = int(lxml_content.xpath('//h1//span')[5].text)
                        if record_check >= 4:
                            customer_mobile = str(
                                lxml_content.xpath('//td[@class="tg-iu1i"]//span')[3].text)
                            customer_name = str(
                                lxml_content.xpath('//td[@class="tg-de5r"]//span')[3].text)
                            customer_cnic = str(
                                lxml_content.xpath('//td[@class="tg-xfje"]//span')[3].text)
                            customer_address = str(
                                lxml_content.xpath('//td[@class="tg-wgsn"]//span')[3].text)
                            customer_operator = str(
                                lxml_content.xpath('//td[@class="tg-0pky"]//span')[19].text)

                            vals_4 = {
                                'customer_mobile': customer_mobile,
                                'customer_name': customer_name,
                                'customer_cnic': customer_cnic,
                                'customer_address': customer_address,
                                'customer_operator': customer_operator
                            }

                            if vals_4:
                                new_record = self.env['scrape.data'].create(vals_4)
                                # print(vals_4)

            print(response.status_code)

    def scrape_with_thread(self):
        thread1 = threading.Thread(target=self.scrape_data)
        thread2 = threading.Thread(target=self.scrape_data)
        thread3 = threading.Thread(target=self.scrape_data)
        thread4 = threading.Thread(target=self.scrape_data)
        thread5 = threading.Thread(target=self.scrape_data)
        thread6 = threading.Thread(target=self.scrape_data)
        thread7 = threading.Thread(target=self.scrape_data)
        thread8 = threading.Thread(target=self.scrape_data)
        thread9 = threading.Thread(target=self.scrape_data)
        thread10 = threading.Thread(target=self.scrape_data)
        thread11 = threading.Thread(target=self.scrape_data)
        thread12 = threading.Thread(target=self.scrape_data)
        thread13 = threading.Thread(target=self.scrape_data)
        thread14 = threading.Thread(target=self.scrape_data)
        thread15 = threading.Thread(target=self.scrape_data)
        thread16 = threading.Thread(target=self.scrape_data)
        thread17 = threading.Thread(target=self.scrape_data)
        thread18 = threading.Thread(target=self.scrape_data)
        thread19 = threading.Thread(target=self.scrape_data)
        thread20 = threading.Thread(target=self.scrape_data)

        # Start the threads
        thread1.start()
        thread2.start()
        thread3.start()
        thread4.start()
        thread5.start()
        thread6.start()
        thread7.start()
        thread8.start()
        thread9.start()
        thread10.start()
        thread11.start()
        thread12.start()
        thread13.start()
        thread14.start()
        thread15.start()
        thread16.start()
        thread17.start()
        thread18.start()
        thread19.start()
        thread20.start()

        # Join the self.threads before
        # self moving further
        thread1.join()
        thread2.join()
        thread3.join()
        thread4.join()
        thread5.join()
        thread6.join()
        thread7.join()
        thread8.join()
        thread9.join()
        thread10.join()
        thread11.join()
        thread12.join()
        thread13.join()
        thread14.join()
        thread15.join()
        thread16.join()
        thread17.join()
        thread18.join()
        thread19.join()
        thread20.join()
