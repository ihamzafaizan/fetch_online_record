# -*- coding: utf-8 -*-
{
    'name': "Fetch Online Record",

    'summary': """
       Fetch Online Record.""",

    'description': """
        Fetch Online Record. 
    """,

    'version': '14.0.0.1',
    'category': 'Uncategorized',

    'author': "Muhammad Hamza Faizan",
    'website': "linkedin.com/in/muhammad-hamza-faizan/",
    'license': 'LGPL-3',
    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/scraped_data.xml',
    ],
    # only loaded in demonstration mode
    'demo': [],
    'qweb': [],
    'installable': True,
    'auto_install': False,
}
